package servicetests;

import core.AsyncService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import payment.DTO.PaymentDTO;
import payment.DTO.PaymentResponseDTO;
import payment.DTO.ResponseDTU;
import payment.DTUPaymentService;
import payment.PaymentService;
import payment.messaging.MessageSender;
import payment.receivers.DTUMoneyTransferResponseEventReceiver;
import payment.receivers.DTUPaymentRequestEventReceiver;
import transaction.DTUTransactionRepository;
import transaction.DTUTransactionService;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class HandlePaymentSteps {
    private MessageSender sender;

    private PaymentService service;

    private AsyncService<PaymentDTO> paymentReceiver;

    private AsyncService<PaymentResponseDTO> paymentResponseReceiver;

    private PaymentDTO payment;

    private PaymentResponseDTO paymentResponse;

    public HandlePaymentSteps() {

        sender = mock(MessageSender.class);
        service = new DTUPaymentService(new DTUTransactionService(DTUTransactionRepository.getInstance()), sender);
        paymentReceiver = new DTUPaymentRequestEventReceiver(service);
        paymentResponseReceiver = new DTUMoneyTransferResponseEventReceiver(service);
    }

    @Given("a customer requesting payment")
    public void aCustomerRequestingPayment() {
        payment = new PaymentDTO(UUID.randomUUID().toString(), UUID.randomUUID().toString(), "receiver", 100F);
    }

    @And("a payment is received")
    public void aPaymentIsReceived() {
        paymentReceiver.receive(payment);
    }

    @Then("the  {string} payment event is broadcast")
    public void thePaymentEventIsBroadcast(String arg0) {
        verify(sender).sendMessage(any(PaymentDTO.class), eq("transaction.payment.received." + payment.id));

    }

    @When("the payment event {string} is received")
    public void thePaymentEventIsReceived(String arg0) {
        paymentResponse = new PaymentResponseDTO();
        paymentResponse.id = payment.id;
        paymentResponse.amount = payment.amount;
        paymentResponse.receiver = payment.receiver;
        paymentResponse.token = payment.token;
        paymentResponse.createdDate = "12-02-1993";
        paymentResponse.isRefund = true;
        paymentResponse.status = true;
        paymentResponseReceiver.receive(paymentResponse);
    }

    @Then("the payment succeed")
    public void thePaymentSucceed() {
        verify(sender).sendMessage(any(ResponseDTU.class), eq("transaction.payment.response." + payment.id));
    }
}
