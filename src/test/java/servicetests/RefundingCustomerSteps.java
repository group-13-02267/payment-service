package servicetests;

import core.AsyncService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import payment.DTO.PaymentResponseDTO;
import payment.DTO.RefundDTO;
import payment.DTO.ResponseDTU;
import payment.DTURefundService;
import payment.RefundService;
import payment.messaging.MessageSender;
import payment.receivers.DTUMoneyRefundEventReceiver;
import payment.receivers.DTUMoneyRefundResponseEventReceiver;
import transaction.DTUTransactionRepository;
import transaction.DTUTransactionService;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class RefundingCustomerSteps {

    private MessageSender refundMessageSender;

    private RefundService refundService;

    private AsyncService<RefundDTO> refundEventReceiver;

    private AsyncService<PaymentResponseDTO> refundResponseEventReceiver;

    private RefundDTO payment;

    private PaymentResponseDTO paymentResponse;

    public RefundingCustomerSteps() {
        refundMessageSender = mock(MessageSender.class);
        refundService = new DTURefundService(
                new DTUTransactionService(DTUTransactionRepository.getInstance()),
                refundMessageSender);
        refundEventReceiver = new DTUMoneyRefundEventReceiver(refundService);
        refundResponseEventReceiver = new DTUMoneyRefundResponseEventReceiver(refundService);
    }

    @Given("a customer requesting refund")
    public void aCustomerRequestingRefund() {
        payment = new RefundDTO(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                "receiver",
                100F);
    }

    @And("a refund is received")
    public void aRefundIsReceived() {
        refundEventReceiver.receive(payment);
    }

    @Then("the {string} event is broadcast")
    public void theEventIsBroadcast(String arg0) {
        //verify(refundMessageSender)
        //        .sendMessage(any(PaymentDTO.class), eq("transaction.refund.received." + payment.id));
    }

    @When("the event {string} is received")
    public void theEventIsReceived(String arg0) {
        paymentResponse = new PaymentResponseDTO();
        paymentResponse.id = payment.id;
        paymentResponse.amount = payment.amount;
        paymentResponse.receiver = payment.sender;
        paymentResponse.token = payment.token;
        paymentResponse.createdDate = "12-02-1993";
        paymentResponse.isRefund = true;
        paymentResponse.status = true;
        refundResponseEventReceiver.receive(paymentResponse);
    }

    @Then("the refund succeed")
    public void theRefundSucceed() {
        verify(refundMessageSender)
                .sendMessage(any(ResponseDTU.class), eq("transaction.refund.response." + payment.id));
    }
}
