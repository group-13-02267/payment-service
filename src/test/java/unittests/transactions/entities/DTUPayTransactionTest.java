package unittests.transactions.entities;

import org.junit.Test;
import transaction.entities.DTUPayTransaction;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DTUPayTransactionTest {

    @Test
    public void shouldInstantiateCorrectDTUPayTransaction() {
        String token = UUID.randomUUID().toString();
        String customerId = "customer-id";
        float amount = 100F;
        String merchantId = "merchant-id";
        DTUPayTransaction transaction = new DTUPayTransaction(token, amount, merchantId, customerId);
        assertEquals(token, transaction.getToken());
        assertEquals(customerId, transaction.getCustomerId());
        assertEquals(amount, transaction.getAmount(), 0);
        assertEquals(merchantId, transaction.getMerchantId());
        UUID.fromString(transaction.getId());
    }

    @Test
    public void refundTransaction_whenRefundingTransaction_shouldBeTrue() {
        String token = UUID.randomUUID().toString();
        String customerId = "customer-id";
        float amount = 100F;
        String merchantId = "merchant-id";
        DTUPayTransaction transaction = new DTUPayTransaction(token, amount, merchantId, customerId);
        transaction.refundTransaction();
        assertTrue(transaction.hasBeenRefunded());
    }

}
