package unittests.transactions;

import org.junit.Before;
import org.junit.Test;
import transaction.DTUTransactionService;
import transaction.TransactionRepository;
import transaction.TransactionService;
import transaction.entities.DTUPayTransaction;
import transaction.entities.PayTransaction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class DTUTransactionServiceTest {

    private TransactionRepository repository;

    private TransactionService service;

    @Before
    public void setup() {
        repository = mock(TransactionRepository.class);
        service = new DTUTransactionService(repository);
    }

    @Test
    public void addTransaction_whenAddingTransaction_shouldAdd() {
        PayTransaction transaction
                = new DTUPayTransaction("token", 100F, "merchant", "customer-id");
        service.addTransaction(transaction);
        verify(repository).add(transaction);
    }

    @Test
    public void getTransaction_whenGettingTransaciton_shouldReturnTransaction() {
        String customerId = "customer-id";
        PayTransaction transaction
                = new DTUPayTransaction("token", 100F, "merchant", customerId);

        when(repository.getTransaction(customerId)).thenReturn(transaction);

        PayTransaction payTransaction = service.getTransaction(customerId);

        assertEquals(payTransaction, transaction);
    }

    @Test
    public void getTransaction_whenGettingNotExistingTransaction_shouldReturnNull() {
        String customerId = "customer-id";

        when(repository.getTransaction(customerId)).thenReturn(null);

        PayTransaction payTransaction = service.getTransaction(customerId);

        assertNull(payTransaction);
    }

    @Test
    public void refund_whenRefundingTransaction_shouldReturnRefundedTransaction() {
        String customerId = "customer-id";
        PayTransaction transaction
                = new DTUPayTransaction("token", 100F, "merchant", customerId);
        service.refundTransaction(transaction);
        verify(repository).add(transaction);
    }
}
