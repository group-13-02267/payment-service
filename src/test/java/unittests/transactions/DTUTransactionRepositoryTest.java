package unittests.transactions;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import transaction.DTUTransactionRepository;
import transaction.TransactionRepository;
import transaction.entities.DTUPayTransaction;
import transaction.entities.PayTransaction;

import static org.junit.Assert.assertNotNull;

public class DTUTransactionRepositoryTest {

    private static TransactionRepository repository;

    @BeforeClass
    public static void setupFixture() {
        repository = DTUTransactionRepository.getInstance();
    }

    @After
    public void cleanup() {
        DTUTransactionRepository.clean();
    }

    @Test
    public void add_whenAddingValidTransaction_shouldExistInRepo() {
        PayTransaction transaction
                = new DTUPayTransaction("token", 100F, "merchant", "customer-id");
        repository.add(transaction);
        assertNotNull(repository.getTransaction(transaction.getId()));
    }
}
