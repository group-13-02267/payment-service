package unittests.payment;

import org.junit.Before;
import org.junit.Test;
import payment.DTO.PaymentDTO;
import payment.DTO.ResponseDTU;
import payment.DTUPaymentService;
import payment.PaymentService;
import payment.messaging.MessageSender;
import transaction.TransactionService;
import transaction.entities.DTUPayTransaction;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DTUPaymentServiceTest {

    private MessageSender sender;

    private TransactionService transactionService;

    private PaymentService paymentService;

    @Before
    public void setup() {
        sender = mock(MessageSender.class);
        transactionService = mock(TransactionService.class);
        paymentService = new DTUPaymentService(transactionService, sender);
    }

    @Test
    public void handlePaymentRequest_whenHandlingPaymentRequest_shouldSendMessage() throws Exception {
        String paymentId = "payment-id";
        String token = "token";
        String merchant = "merchant";
        float amount = 100F;

        paymentService.handlePaymentRequest(paymentId, token, merchant, amount);

        verify(sender).sendMessage(any(PaymentDTO.class), eq("transaction.payment.received." + paymentId));
    }

    @Test
    public void handlePaymentResponse_whenHandlingPaymentResponse_shouldSendMessageAndAddTransaction() throws Exception {
        String paymentId = "payment-id";
        String token = "token";
        String merchant = "merchant";
        String customer = "customer";
        boolean status = true;
        float amount = 100F;

        paymentService.handlePaymentResponse(paymentId, customer, merchant, amount, status, token);

        verify(transactionService).addTransaction(any(DTUPayTransaction.class));
        verify(sender).sendMessage(any(ResponseDTU.class), eq("transaction.payment.response." + paymentId));
    }

    @Test
    public void handlePaymentResponse_whenHandlingInvalidPaymentResponse_shouldSendMessageAndNotAddTransaction() throws Exception {
        String paymentId = "payment-id";
        String token = "token";
        String merchant = "merchant";
        String customer = "customer";
        boolean status = false;
        float amount = 100F;

        paymentService.handlePaymentResponse(paymentId, customer, merchant, amount, status, token);

        verify(transactionService, never()).addTransaction(any(DTUPayTransaction.class));
        verify(sender).sendMessage(any(ResponseDTU.class), eq("transaction.payment.response." + paymentId));
    }
}
