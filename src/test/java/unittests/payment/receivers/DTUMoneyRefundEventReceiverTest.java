package unittests.payment.receivers;

import org.junit.Before;
import org.junit.Test;
import payment.DTO.RefundDTO;
import payment.RefundService;
import payment.receivers.DTUMoneyRefundEventReceiver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DTUMoneyRefundEventReceiverTest {

    private RefundService refundService;

    private DTUMoneyRefundEventReceiver eventReceiver;

    @Before
    public void setup() {
        refundService = mock(RefundService.class);
        eventReceiver = new DTUMoneyRefundEventReceiver(refundService);
    }

    @Test
    public void receive_whenReceivingPaymentDTO_shouldRequestRefund() {
        String id = "id";
        String token = "token";
        String sender = "receiver";
        float amount = 100F;
        RefundDTO dto = new RefundDTO(id, token, sender, amount);
        eventReceiver.receive(dto);
        verify(refundService).requestRefund(id, token, sender, amount);
    }

}
