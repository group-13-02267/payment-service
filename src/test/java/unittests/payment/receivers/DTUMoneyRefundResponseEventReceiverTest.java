package unittests.payment.receivers;

import core.AsyncService;
import org.junit.Before;
import org.junit.Test;
import payment.DTO.PaymentResponseDTO;
import payment.RefundService;
import payment.receivers.DTUMoneyRefundResponseEventReceiver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DTUMoneyRefundResponseEventReceiverTest {

    private RefundService refundService;

    private AsyncService<PaymentResponseDTO> service;

    @Before
    public void setup() {
        refundService = mock(RefundService.class);
        service = new DTUMoneyRefundResponseEventReceiver(refundService);
    }

    @Test
    public void receive_whenReceivingPaymentDTO_shouldRequestRefund() {
        String id = "id";
        String token = "token";
        String receiver = "receiver";
        float amount = 100F;
        boolean status = true;

        PaymentResponseDTO dto = new PaymentResponseDTO();
        dto.id = id;
        dto.token = token;
        dto.receiver = receiver;
        dto.amount = amount;
        dto.status = status;
        dto.isRefund = true;
        dto.createdDate = "12-02-1993";
        service.receive(dto);

        verify(refundService).refund(dto.id, dto.token, dto.receiver, dto.sender, dto.status, dto.amount);
    }
}
