package unittests.payment.receivers;

import core.AsyncService;
import org.junit.Before;
import org.junit.Test;
import payment.DTO.PaymentDTO;
import payment.PaymentService;
import payment.receivers.DTUPaymentRequestEventReceiver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DTUPaymentRequestEventReceiverTest {

    private PaymentService paymentService;

    private AsyncService<PaymentDTO> eventReceiver;

    @Before
    public void setup() {
        paymentService = mock(PaymentService.class);
        eventReceiver = new DTUPaymentRequestEventReceiver(paymentService);
    }

    @Test
    public void receive_whenReceivingPaymentDTO_shouldSendMessage() throws Exception {
        PaymentDTO payment = new PaymentDTO("id", "token", "receiver", 100F);

        eventReceiver.receive(payment);

        verify(paymentService).handlePaymentRequest(payment.id, payment.token, payment.receiver, payment.amount);
    }

}
