package unittests.payment.receivers;

import core.AsyncService;
import org.junit.Before;
import org.junit.Test;
import payment.DTO.PaymentResponseDTO;
import payment.PaymentService;
import payment.receivers.DTUMoneyTransferResponseEventReceiver;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DTUMoneyTransferResponseEventReceiverTest {
    private PaymentService paymentService;

    private AsyncService<PaymentResponseDTO> service;

    @Before
    public void setup() {
        paymentService = mock(PaymentService.class);
        service = new DTUMoneyTransferResponseEventReceiver(paymentService);
    }

    @Test
    public void receive_whenReceivingPaymentDTO_shouldRequestRefund() throws Exception {
        String id = "id";
        String token = "token";
        String receiver = "receiver";
        float amount = 100F;
        boolean status = true;

        PaymentResponseDTO dto = new PaymentResponseDTO();
        dto.id = id;
        dto.token = token;
        dto.receiver = receiver;
        dto.amount = amount;
        dto.status = status;
        dto.isRefund = true;
        dto.createdDate = "12-02-1993";
        service.receive(dto);

        verify(paymentService).handlePaymentResponse(dto.id, dto.sender, dto.receiver, dto.amount, dto.status, dto.token);
    }
}
