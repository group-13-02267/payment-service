package unittests.payment;

import org.junit.Before;
import org.junit.Test;
import payment.DTO.ResponseDTU;
import payment.DTURefundService;
import payment.RefundService;
import payment.messaging.MessageSender;
import transaction.TransactionService;
import transaction.entities.DTUPayTransaction;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DTURefundServiceTest {
    private MessageSender sender;

    private TransactionService transactionService;

    private RefundService paymentService;

    @Before
    public void setup() {
        sender = mock(MessageSender.class);
        transactionService = mock(TransactionService.class);
        paymentService = new DTURefundService(transactionService, sender);
    }

    @Test
    public void handlePaymentRequest_whenHandlingPaymentRequest_shouldSendMessage() throws Exception {
        String id = "payment-id";
        String token = "token";
        String merchant = "merchant";
        float amount = 100F;

        //paymentService.requestRefund(id, token, merchant, amount);

        //verify(sender).sendMessage(any(PaymentDTO.class), eq("transaction.refund.received." + id));
    }

    @Test
    public void handlePaymentResponse_whenHandlingPaymentResponse_shouldSendMessageAndAddTransaction() throws Exception {
        String id = "refund-id";
        String token = "token";
        String merchant = "merchant";
        String customer = "customer";
        boolean status = true;
        float amount = 100F;

        paymentService.refund(id, token, customer, merchant, status, amount);

        verify(transactionService).refundTransaction(any(DTUPayTransaction.class));
        verify(sender).sendMessage(any(ResponseDTU.class), eq("transaction.refund.response." + id));
    }

    @Test
    public void handlePaymentResponse_whenHandlingInvalidPaymentResponse_shouldSendMessageAndNotAddTransaction() throws Exception {
        String id = "refund-id";
        String token = "token";
        String merchant = "merchant";
        String customer = "customer";
        boolean status = false;
        float amount = 100F;

        paymentService.refund(id, token, customer, merchant, status, amount);

        verify(transactionService, never()).refundTransaction(any(DTUPayTransaction.class));
        verify(sender).sendMessage(any(ResponseDTU.class), eq("transaction.refund.response." + id));
    }
}
