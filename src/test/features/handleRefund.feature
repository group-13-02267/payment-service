Feature: Refund Feature

  Scenario: Refunding Customer
    Given a customer requesting refund
    And a refund is received
    Then the "transaction.refund.received.*" event is broadcast
    When the event "transaction.refund.handled.*" is received
    Then the refund succeed
