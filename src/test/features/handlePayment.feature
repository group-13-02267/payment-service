Feature: Payment Feature

  Scenario: Payment Received
    Given a customer requesting payment
    And a payment is received
    Then the  "transaction.payment.received.*" payment event is broadcast
    When the payment event "transaction.payment.handled.*" is received
    Then the payment succeed
