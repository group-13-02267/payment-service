package transaction;

import transaction.entities.PayTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Transaction Repository implementation used to store transactions
 *
 * @author Mathias Ibsen
 */
public class DTUTransactionRepository implements TransactionRepository {

    /**
     * The Transaction repository instance
     */
    private static DTUTransactionRepository instance;

    /**
     * List containing all the transactions
     */
    List<PayTransaction> payTransactions = new ArrayList<>();

    private DTUTransactionRepository() {
    }

    /**
     * Get the repository instance
     *
     * @return The repository instance
     */
    public static TransactionRepository getInstance() {
        if (instance == null) {
            instance = new DTUTransactionRepository();
        }
        return instance;
    }

    /***
     * Deletes all stored data.
     */
    public static void clean() {
        instance.payTransactions = new ArrayList<>();
    }

    @Override
    public PayTransaction add(PayTransaction transaction) {
        payTransactions.add(transaction);
        return transaction;
    }

    @Override
    public PayTransaction getTransaction(String transactionId) {
        return payTransactions.stream().filter(p -> p.getId().equals(transactionId)).findFirst().orElse(null);
    }
}
