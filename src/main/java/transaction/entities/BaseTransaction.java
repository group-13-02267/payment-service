package transaction.entities;

/**
 * Interface for base transactions
 */
public interface BaseTransaction {
    /**
     * Get the token used for the transaction
     *
     * @return The raw token value
     */
    String getToken();

    /**
     * Get the transaction amount
     *
     * @return The transaction amount
     */
    float getAmount();
}
