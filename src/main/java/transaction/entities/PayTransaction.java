package transaction.entities;

import java.time.LocalDate;

/**
 * @author Mathias Ibsen
 */
public interface PayTransaction extends BaseTransaction {

    /**
     * Get the transaction id
     *
     * @return The transaction id
     */
    String getId();

    /**
     * Get the merchant id
     *
     * @return The merchant id
     */
    String getMerchantId();

    /**
     * Get the customer id
     *
     * @return The customer id
     */
    String getCustomerId();

    /**
     * Get the transaction creation date
     *
     * @return The creation date as dd-mm-yyyy
     */
    LocalDate getDate();

    /**
     * Status if the transaction has been refunded
     *
     * @return True if refunded, false if not
     */
    boolean hasBeenRefunded();

    /**
     * Refund the transaction
     */
    void refundTransaction();
}
