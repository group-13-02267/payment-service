package transaction.entities;

import java.time.LocalDate;
import java.util.UUID;

/**
 * PayTransaction implementation used to store all the transaction information
 *
 * @author Mathias Ibsen
 */
public class DTUPayTransaction implements PayTransaction {

    /**
     * The unique transaction id
     */
    private String id;

    /**
     * The merchant of the transaction
     */
    private String merchantId;

    /**
     * The customer of the transaction
     */
    private String customerId;

    /**
     * Boolean representing true if the transaction has been refunded
     */
    private boolean hasBeenRefunded;

    /**
     * The token used for the transaction
     */
    private String token;

    /**
     * The transaction amount
     */
    private float amount;

    /**
     * The transacton creation date
     */
    private LocalDate creationDate;

    /**
     * Create a new Transaction
     * @param token The token used for the transaction
     * @param amount The transaction amount
     * @param merchantId The transaction merchant
     * @param customerId The customer
     */
    public DTUPayTransaction(String token, float amount, String merchantId, String customerId) {
        this.token = token;
        this.amount = amount;
        this.merchantId = merchantId;
        this.customerId = customerId;
        this.id = UUID.randomUUID().toString();
        this.hasBeenRefunded = false;
        creationDate = LocalDate.now();
    }

    protected DTUPayTransaction() {
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getMerchantId() {
        return merchantId;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }

    @Override
    public LocalDate getDate() {
        return creationDate;
    }

    @Override
    public boolean hasBeenRefunded() {
        return hasBeenRefunded;
    }

    @Override
    public void refundTransaction() {
        hasBeenRefunded = true;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public float getAmount() {
        return amount;
    }
}
