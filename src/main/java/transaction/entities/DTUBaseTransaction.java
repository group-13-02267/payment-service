package transaction.entities;

/**
 * Abstract Base Transaction
 *
 * @author Mathias Ibsen
 */
public abstract class DTUBaseTransaction implements BaseTransaction {

    /**
     * The token used for the transaction
     */
    private String token;

    /**
     * The transaction amount
     */
    private float amount;

    protected DTUBaseTransaction() {
    }

    public DTUBaseTransaction(String token, float amount) {
        this.token = token;
        this.amount = amount;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public float getAmount() {
        return amount;
    }
}
