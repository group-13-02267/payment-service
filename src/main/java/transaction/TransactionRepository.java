package transaction;

import transaction.entities.PayTransaction;

/**
 * Interface for repositories used to store transactions
 *
 * @author Mathias Ibsen
 */
public interface TransactionRepository {

    /**
     * Store a transaction
     *
     * @param transaction The transaction to add
     * @return The enriched transaction
     */
    PayTransaction add(PayTransaction transaction);

    /**
     * Get a stored transaction by id
     *
     * @param transactionId The transaction id
     * @return The transaction with the id, null if no transaction was found
     */
    PayTransaction getTransaction(String transactionId);
}
