package transaction;

import transaction.entities.PayTransaction;

/**
 * Transaction service implementation for handling transactions
 *
 * @author Mathias Ibsen
 */
public class DTUTransactionService implements TransactionService {

    /**
     * The transaction repository that holds all the transactions
     */
    private final TransactionRepository transactionRepository;

    /**
     * Instantiate a new Transaction Service
     * @param transactionRepository The repository used to store transactions
     */
    public DTUTransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public void addTransaction(PayTransaction transaction) {
        transactionRepository.add(transaction);
    }

    @Override
    public void refundTransaction(PayTransaction transaction) {
        transaction.refundTransaction();
        transactionRepository.add(transaction);
    }

    @Override
    public PayTransaction getTransaction(String transactionId) {
        return transactionRepository.getTransaction(transactionId);
    }
}
