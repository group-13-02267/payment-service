package transaction;

import transaction.entities.PayTransaction;

/**
 * Interface for Transaction Services
 *
 * @author Mathias Ibsen
 */
public interface TransactionService {
    /**
     * Add a new transaction
     *
     * @param transaction The new transaction
     */
    void addTransaction(PayTransaction transaction);

    /**
     * Refund a transaction
     *
     * @param transaction The transaction to refund
     */
    void refundTransaction(PayTransaction transaction);

    /**
     * Get transaction by id
     *
     * @param transactionId The id of the transaction
     * @return The transaction with the id, null if it does not exist
     */
    PayTransaction getTransaction(String transactionId);

}
