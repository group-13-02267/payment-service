package payment;


/**
 * Interface for requesting and responding to payments
 *
 * @author Mathias
 */
public interface PaymentService {
    /**
     * Handle a payment request
     *
     * @param paymentId  The unique payment id
     * @param token      The token used for the payment
     * @param merchantId The merchant id
     * @param amount     The payment amount
     * @throws Exception Thrown if the payment request fails
     */
    void handlePaymentRequest(String paymentId, String token, String merchantId, float amount) throws Exception;

    /**
     * Handle a payment response
     *
     * @param paymentId  The unique payment id
     * @param userId     The customer id
     * @param merchantId The merchant id
     * @param amount     The payment amount
     * @param status     The validation status
     * @param token      The token used for the payment
     * @throws Exception Thrown if the payment request fails
     */
    void handlePaymentResponse(String paymentId, String userId, String merchantId, float amount, boolean status, String token) throws Exception;
}
