package payment;

import payment.DTO.RefundDTO;
import payment.DTO.ResponseDTU;
import payment.messaging.MessageSender;
import transaction.TransactionService;
import transaction.entities.DTUPayTransaction;

/**
 * Service for refunding transactions
 *
 * @author David
 */
public class DTURefundService implements RefundService {

    /**
     * The service for handling transactions
     */
    private final TransactionService service;

    /**
     * The message sender
     */
    private final MessageSender messageSender;

    /**
     * Instantiate a new PaymentService
     *
     * @param service       The transaction service used to handle transactions
     * @param messageSender The message sender used to broadcast events
     */
    public DTURefundService(TransactionService service, MessageSender messageSender) {
        this.service = service;
        this.messageSender = messageSender;
    }

    @Override
    public void requestRefund(String id, String token, String merchant, float amount) {
        String route = "transaction.refund.received." + id;
        RefundDTO payload = new RefundDTO(id, token, merchant, amount);
        messageSender.sendMessage(payload, route);
    }

    @Override
    public void refund(String id, String token, String customer, String merchant, boolean status, float amount) {
        if (status) {
            service.refundTransaction(new DTUPayTransaction(token, amount, merchant, customer));
        }
        messageSender.sendMessage(new ResponseDTU(status), "transaction.refund.response." + id);
    }
}
