package payment.DTO;

import java.io.Serializable;

/**
 * Payment request DTO
 *
 * @author David
 */
public class PaymentDTO implements Serializable {

    public PaymentDTO(String id, String token, String receiver, float amount) {
        this.id = id;
        this.token = token;
        this.receiver = receiver;
        this.amount = amount;
    }

    public PaymentDTO() {
    }

    /**
     * The unique request id
     */
    public String id;

    /**
     * The token used for the transaction request
     */
    public String token;

    /**
     * The transaction receiver
     */
    public String receiver;

    /**
     * The transaction amount
     */
    public float amount;
}
