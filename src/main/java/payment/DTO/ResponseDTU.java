package payment.DTO;

import java.io.Serializable;

public class ResponseDTU implements Serializable {

    public ResponseDTU(boolean success) {
        this.success = success;
    }

    public ResponseDTU() {
    }

    /**
     * Transaction status
     */
    public boolean success;
}
