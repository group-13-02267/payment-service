package payment.DTO;

/**
 * DTO for refunds
 */
public class RefundDTO {

    /**
     * The request id
     */
    public String id;

    /**
     * The token used for the refund
     */
    public String token;

    /**
     * The sender id
     */
    public String sender;

    /**
     * The refund amount
     */
    public float amount;

    public RefundDTO(String id, String token, String sender, float amount) {
        this.id = id;
        this.token = token;
        this.sender = sender;
        this.amount = amount;
    }

    public RefundDTO() {
    }

}
