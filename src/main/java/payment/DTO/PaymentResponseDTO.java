package payment.DTO;

import java.io.Serializable;

/**
 * Payment Response DTO
 *
 * @author Georg
 */
public class PaymentResponseDTO implements Serializable {

    /**
     * The payment validation status
     */
    public boolean status;

    /**
     * The unique request id
     */
    public String id;

    /**
     * The transaction sender
     */
    public String sender;

    /**
     * The transaction used
     */
    public String token;

    /**
     * The transaction receiver
     */
    public String receiver;

    /**
     * The transaction amount
     */
    public float amount;

    /**
     * Is the transaction a refund - then true.
     * If the transaction is a payment - then false.
     */
    public boolean isRefund;

    /**
     * The creation as dd-mm-yyyy
     */
    public String createdDate;
}
