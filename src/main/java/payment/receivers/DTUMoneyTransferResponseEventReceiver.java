package payment.receivers;

import com.google.gson.Gson;
import core.AsyncService;
import payment.DTO.PaymentResponseDTO;
import payment.PaymentService;

/**
 * Money Transfer Response Event Receiver
 *
 * @author Mathias
 */
public class DTUMoneyTransferResponseEventReceiver extends AsyncService<PaymentResponseDTO> {

    /**
     * The payment service responsible for handling payments
     */
    private final PaymentService paymentService;

    /**
     * Instantiate a new Money Transfer Response Event Receiver
     * @param paymentService The payment service for handling payments
     */
    public DTUMoneyTransferResponseEventReceiver(PaymentService paymentService) {
        super("payment", "transaction.payment.handled.*");
        this.paymentService = paymentService;
    }

    @Override
    protected PaymentResponseDTO handleMessage(String message) {
        return new Gson().fromJson(message, PaymentResponseDTO.class);
    }

    @Override
    public void receive(PaymentResponseDTO received) {
        try {
            paymentService.handlePaymentResponse(received.id, received.sender, received.receiver,
                    received.amount, received.status, received.token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
