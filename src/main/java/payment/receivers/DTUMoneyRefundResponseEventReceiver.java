package payment.receivers;

import com.google.gson.Gson;
import core.AsyncService;
import payment.DTO.PaymentResponseDTO;
import payment.RefundService;

public class DTUMoneyRefundResponseEventReceiver extends AsyncService<PaymentResponseDTO> {

    /**
     * The exchange used to receive the refund response events on
     */
    private static final String exchange = "payment";

    /**
     * The refund service
     */
    private final RefundService service;

    /**
     * Instantiate a new refund response event receiver
     *
     * @param refundService The refund service to handle refund
     */
    public DTUMoneyRefundResponseEventReceiver(RefundService refundService) {
        super(exchange, "transaction.refund.handled.*");
        service = refundService;
    }

    @Override
    protected PaymentResponseDTO handleMessage(String s) {
        return new Gson().fromJson(s, PaymentResponseDTO.class);
    }

    @Override
    public void receive(PaymentResponseDTO paymentResponseDTO) {
        service.refund(paymentResponseDTO.id,
                paymentResponseDTO.token,
                paymentResponseDTO.receiver,
                paymentResponseDTO.sender,
                paymentResponseDTO.status,
                paymentResponseDTO.amount);
    }
}
