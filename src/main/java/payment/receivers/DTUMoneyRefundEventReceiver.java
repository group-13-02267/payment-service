package payment.receivers;

import com.google.gson.Gson;
import core.AsyncService;
import payment.DTO.RefundDTO;
import payment.RefundService;

/**
 * Event for receiving the refund request
 *
 * @author David
 */
public class DTUMoneyRefundEventReceiver extends AsyncService<RefundDTO> {

    /**
     * The exchange that the refund requests is received on
     */
    private static final String exchange = "payment";

    /**
     * The refund service responsible
     */
    private final RefundService refundService;

    /**
     * Instantiate a new refund event receiver
     *
     * @param refundService The refund service to handle refund
     */
    public DTUMoneyRefundEventReceiver(RefundService refundService) {
        super(exchange, "transaction.refund.requested.*");
        this.refundService = refundService;
    }

    @Override
    protected RefundDTO handleMessage(String s) {
        return new Gson().fromJson(s, RefundDTO.class);
    }

    @Override
    public void receive(RefundDTO paymentDTO) {
        refundService.requestRefund(paymentDTO.id, paymentDTO.token, paymentDTO.sender, paymentDTO.amount);
    }
}
