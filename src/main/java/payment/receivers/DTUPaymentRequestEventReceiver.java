package payment.receivers;

import com.google.gson.Gson;
import core.AsyncService;
import payment.DTO.PaymentDTO;
import payment.PaymentService;


/**
 * Requesting payment asynchronously
 *
 * @author Mathias
 */
public class DTUPaymentRequestEventReceiver extends AsyncService<PaymentDTO> {

    /**
     * The payment service responsible for handling the payment requests
     */
    private final PaymentService paymentService;

    /**
     * Instantiate a new payment request event receiver
     *
     * @param paymentService The payment service used to handle payments
     */
    public DTUPaymentRequestEventReceiver(PaymentService paymentService) {
        super("payment", "transaction.payment.requested.*");
        this.paymentService = paymentService;
    }

    @Override
    protected PaymentDTO handleMessage(String message) {
        return new Gson().fromJson(message, PaymentDTO.class);
    }

    @Override
    public void receive(PaymentDTO received) {
        try {
            paymentService.handlePaymentRequest(received.id, received.token, received.receiver, received.amount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
