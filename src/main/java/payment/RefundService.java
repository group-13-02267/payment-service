package payment;

/**
 * Interface for refunding
 *
 * @author David
 */
public interface RefundService {

    /**
     * Request a refund
     *
     * @param id       The unique refund request id
     * @param token    The token used to refund
     * @param merchant The merchant id
     * @param amount   The refund amount
     */
    void requestRefund(String id, String token, String merchant, float amount);

    /**
     * Handle a refund
     *
     * @param id       The unique refund request id
     * @param token    The token used to refund
     * @param customer The customer id
     * @param merchant The merchant id
     * @param status   The refund validation status
     * @param amount   The refund amount
     */
    void refund(String id, String token, String customer, String merchant, boolean status, float amount);
}
