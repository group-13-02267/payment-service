package payment.messaging;

import core.AsyncSender;
import core.RabbitSender;

/**
 * Asynchronous message sender implementation for sending messages using RabbitMQ
 */
public class RabbitMqMessageSenderService implements MessageSender {
    @Override
    public void sendMessage(Object content, String route) {
        AsyncSender eventSender = new RabbitSender("payment", route);
        eventSender.send(content);
    }
}
