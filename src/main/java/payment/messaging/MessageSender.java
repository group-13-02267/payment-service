package payment.messaging;

/**
 * Interface for sending messages
 */
public interface MessageSender {
    void sendMessage(Object content, String route);
}
