package payment;

import payment.DTO.PaymentDTO;
import payment.DTO.ResponseDTU;
import payment.messaging.MessageSender;
import transaction.TransactionService;
import transaction.entities.DTUPayTransaction;

/**
 * PaymentService implementation for handling payments
 *
 * @author Mathias
 */
public class DTUPaymentService implements PaymentService {

    /**
     * The service for handling transactions
     */
    private final TransactionService transactionService;

    /**
     * The message sender
     */
    private final MessageSender messageSender;

    /**
     * Instantiate a new PaymentService
     * @param transactionService The transaction service used to handle transactions
     * @param messageSender The message sender used to broadcast events
     */
    public DTUPaymentService(TransactionService transactionService, MessageSender messageSender) {
        this.transactionService = transactionService;
        this.messageSender = messageSender;
    }

    @Override
    public void handlePaymentRequest(String paymentId, String token, String merchantId, float amount) throws Exception {
        PaymentDTO paymentDTO = new PaymentDTO(paymentId, token, merchantId, amount);
        messageSender.sendMessage(paymentDTO, "transaction.payment.received." + paymentId);
    }

    @Override
    public void handlePaymentResponse(String paymentId, String customerId, String merchantId, float amount, boolean status, String token) throws Exception {
        if (status){
            transactionService.addTransaction(new DTUPayTransaction(token, amount, merchantId, customerId));
        }
        messageSender.sendMessage(new ResponseDTU(status), "transaction.payment.response." + paymentId);
    }
}
