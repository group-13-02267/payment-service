package runtime;

import core.AsyncException;
import core.QueueConfiguration;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author David
 */
public abstract class Microservice implements Closeable {

    /**
     * The asynchronous queue connection configuration
     */
    protected final QueueConfiguration queueConfiguration;

    protected Microservice() {
        this.queueConfiguration = new QueueConfiguration();
    }

    /**
     * Startup configuration for the microservice
     *
     * @throws AsyncException Thrown if any asynchronous services fails connecting
     */
    public abstract void startup() throws AsyncException;

    /**
     * Called when the microservice closes
     *
     * @throws IOException Thrown if an IO error happens while closing
     */
    @Override
    public void close() throws IOException {

    }
}
