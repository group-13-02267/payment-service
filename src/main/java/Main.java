import core.AsyncException;
import core.AsyncService;
import payment.DTO.PaymentResponseDTO;
import payment.DTO.RefundDTO;
import payment.DTUPaymentService;
import payment.DTURefundService;
import payment.PaymentService;
import payment.RefundService;
import payment.messaging.MessageSender;
import payment.messaging.RabbitMqMessageSenderService;
import payment.receivers.DTUMoneyRefundEventReceiver;
import payment.receivers.DTUMoneyRefundResponseEventReceiver;
import payment.receivers.DTUMoneyTransferResponseEventReceiver;
import payment.receivers.DTUPaymentRequestEventReceiver;
import runtime.Microservice;
import transaction.DTUTransactionRepository;
import transaction.DTUTransactionService;
import transaction.TransactionService;

public class Main extends Microservice {
    public static void main(String[] args) throws Exception {
        Microservice service = new Main();
        System.out.println("Starting Payment Service...");
        service.startup();
    }

    @Override
    public void startup() throws AsyncException {
        TransactionService transactionService = new DTUTransactionService(DTUTransactionRepository.getInstance());
        MessageSender sender = new RabbitMqMessageSenderService();
        PaymentService paymentService = new DTUPaymentService(transactionService, sender);
        RefundService refundService = new DTURefundService(transactionService, sender);

        DTUMoneyTransferResponseEventReceiver moneyTransferResponseEventReceiver
                = new DTUMoneyTransferResponseEventReceiver(paymentService);
        moneyTransferResponseEventReceiver.listen();

        DTUPaymentRequestEventReceiver paymentRequestEventReceiver = new DTUPaymentRequestEventReceiver(paymentService);
        paymentRequestEventReceiver.listen();

        AsyncService<RefundDTO> refundRequestEventReceiver = new DTUMoneyRefundEventReceiver(refundService);
        refundRequestEventReceiver.listen();

        AsyncService<PaymentResponseDTO> refundResponseEventReceiver
                = new DTUMoneyRefundResponseEventReceiver(refundService);
        refundResponseEventReceiver.listen();
    }
}
